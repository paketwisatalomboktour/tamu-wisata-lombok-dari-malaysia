Sebagai salah satu jasa paket wisata Lombok yang profesional, kami dari First Lombok Tour berusaha untuk melayani setiap tamu dengan baik. Tak terkecuali tamu yang berasal dari luar negeri. Lombok memang sudah bukan lagi hanya tujuan wisata nasional, namun namanya sudah terkenal hingga mancanegera. Maka tak jarang kita menemukan banyak review travel blogger internasional tentang lombok, seperti tulisan ini, dan masih banyak lagi tentunya. Alhamdulillah kami mendapatkan kesempatan untuk melayani tamu dari negeri tetangga. Sebanyak 32 orang dari Malaysia datang ke Lombok dan berkeliling untuk liburan bersama kami. Seperti apa aktivitasnya, simak ulasan dibawah ini.

Day 1 : [Kedatangan Tamu dari Malaysia](https://firstlomboktour.com/dokumentasi/tour-lombok-tamu-dari-malaysia)
Rombongan dari Malaysia tiba di Bandara Internasional Lombok pada Pukul 18.00 WITA. Sambutan oleh tim travel First Lombok sangat ramah. Dengan armada yang prima bus 35 seat dan guide yang sangat ramah, kami menyambut rombongan tersebut. Di hari pertama kami tidak melakukan tour, dikarenakan kedatangan tamu pada malam hari. Kami dan rombongan langsung melakukan aktifitas dinner, dimana Restaurant yang kami rekomendasikan insyaa Allah sangat nyaman, bersih serta makanannya rekomend banget untuk rombongan.

Tour lombok tamu dari malaysia - berfoto di tanjung aan
Berfoto di Tanjung Aan Lombok

Day 2 : Mengenal Budaya Lombok
Hari ke 2, kami mengajak rombongan untuk mengenal budaya lokal Lombok dan pantai-pantainya yang berada di wilayah selatan Lombok. Destinasi pertama kami yaitu Desa Sukarara, sukarara merupakan desa tenun khas Lombok, dimana kami bisa melakukan aktifitas tenun yang biasa dilakukan oleh wanita Lombok, di tempat ini juga kami bisa berfoto-foto menggunakan baju adat Lombok, kami berusaha membuat rombongan happy, karena di negara Malaysia tidak ada aktifitas seperti ini sehingga hal tersebut bisa menjadi sesuatu yang amat menarik bagi tamu.

tour tamu dari malaysia - berfoto di lumbung padi lombok
Berfoto di lumbung padi khas Lombok

Destinasi berikutnya kami mengajak rombongan mengunjungi Desa Sade, di desa Sade kami disambut dengan adat budaya peresean, peresean ini merupakan adat seni yang di lakukan oleh pria Lombok, untuk memohon hujan.

[paket wisata lombok](https://firstlomboktour.wixsite.com/paketwisatalombok) mengantar tamu dari malaysia - melihat perang adat suku sasak
Paket wisata lombok mengantar tamu dari Malaysia – melihat peresean

Di Desa ini juga kami mengajak rombongan berkeliling melihat rumah adat lombok, katanya lantai dirumah ini di pel lantainya 3 bulan sekali menggunakan kotoran kerbau, untungnya saat berkunjung aku nggak nemu. Haha Destinasi berikutnya kami dan rombongan adalah berkunjung ke pantai kuta Lombok, satu kata untuk pantai ini “Luar biasa” laut yang berwarna biru serta pasirnya yang putih membuat hati terasa senang banget.

Day 3 : Menikmati 3 Gili
Destinasi kami selanjutnya berkunjung ke Gili Trawangan, dari pelabuhan teluk Nara kami menggunakan Speedboat yang hanya menempuh 10 menit, alhamdulillah cuaca saat itu sangat bersahabat. Di gili kami melakukan aktifitas snorkeling ke, gili air dan meno, kami bisa melihat terumbu karang yang indah serta ikan nemo dan ikan lainnya.

Paket wisata lombok tamu malaysia - snorkeling di Gili Trawangan
Tamu snorkeling di Gili Trawangan Lombok

Setelah [aktifitas snorkeling](https://mobillombok.com/wisata/tempat-snorkeling-di-lombok.html), kami mempersilahkan rombongan untuk mencoba menggunakan sepeda untuk berkeliling di Gili trawangan, banyak tamu rombongan dari Malaysia ini mencari ayunan ombak sunset, karna spot wisata tersebut sangat terkenal di sosial media, sebagai tourist  tak lupa juga para rombongan juga berfoto-foto serta menikmati indahnya pemandangan gili trawangan.

Day 4 : City Tour di Mataram
Hari terakhir kami di Lombok, kami dari tim First Lombok Tour mengajak rombongan untuk mengunjungi Masjid Islamic center mataram, tempat ini salah satu icon kota Mataram, di masjid ini kami mengajak tamu dari Malaysia mencoba menaiki tower yang ketinggiannya 99 meter, dari tempat setinggi ini rombongan sudah bisa menikmati pemandangan Lombok. Sungguh luar biasa..

paket tour lombok tamu malaysia berfoto bersama dan bahagia liburan 4 hari 3 malam di Lombok
Tamu rombongan berfoto bersama [https://code.videolan.org/snippets/38](https://code.videolan.org/snippets/38)

Itulah sepenggal kisah liburan 4 hari tamu dari Malysia saat berwisata di Lombok bersama kami. Kami sangat bersyukur, kehadiran kami bisa membuat bahagia banyak orang. Sampai jumpa dengan kami jika berlibur di Lombok. Siapa tahu nanti kita berjodoh untuk bertemu di Lombok. Salam hangat dari First Lombok Tour.